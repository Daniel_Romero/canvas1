$(document).ready(function(){

  console.log("Welcome to jQuery.");

  var c = $("#canvas");
  var ctx = c[0].getContext('2d');
  ctx.lineWidth = 2;
  ctx.beginPath();
  ctx.moveTo(150,70);
  ctx.lineTo(150,40);

  ctx.moveTo(150,70);
  ctx.lineTo(120,55);

  ctx.moveTo(150,70);
  ctx.lineTo(120,85);

  ctx.moveTo(150,70);
  ctx.lineTo(150,100);

  ctx.moveTo(150,70);
  ctx.lineTo(180,85);

  ctx.moveTo(150,70);
  ctx.lineTo(180,55);
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(150,70,30,0.75*Math.PI,1.75*Math.PI);
  ctx.arc(150,70,30,1.75*Math.PI,0.75*Math.PI);
  ctx.stroke();

});
